sh ./gradlew buildJar
sudo mkdir /usr/local/bin/notekraken
sudo mv ./build/libs/*.jar /usr/local/bin/notekraken/notekraken.jar
sudo cp ./NoteKraken.png /usr/local/bin/notekraken/NoteKraken.png
sudo cp ./NoteKraken.desktop ~/.local/share/applications/notekraken.desktop
desktop-file-install --dir=$HOME/.local/share/applications NoteKraken.desktop
update-desktop-database ~/.local/share/applications