import java.awt.BorderLayout
import java.awt.Dimension
import java.awt.FlowLayout
import java.awt.Image
import javax.swing.ImageIcon
import javax.swing.JFrame
import javax.swing.JLabel

object AboutWindow : JFrame("About NoteKraken"){
    init {
        minimumSize = Dimension(400,245)
        maximumSize = Dimension(400,245)
        isResizable = false
        isVisible = false
        layout = FlowLayout()

        val logo = ImageIcon(this::class.java.getResource("/logo.png"))

        val logoLabel = JLabel(logo)
        logoLabel.minimumSize = Dimension(150,150)
        logoLabel.maximumSize = Dimension(150,150)

        add(logoLabel)
        add(JLabel("Current Version: $buildNumber"))
        add(JLabel("NoteKraken logo (c) Austin Elkin 2021"))
        add(JLabel("Design/Programming (c) Rickey Chamblee 2021"))

        setLocationRelativeTo(MainScreen)
    }

    fun open() {
        isVisible = true
    }
}