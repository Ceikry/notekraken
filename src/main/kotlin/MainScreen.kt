import org.json.simple.JSONObject
import java.awt.BorderLayout
import java.awt.Dimension
import java.awt.KeyboardFocusManager
import java.awt.event.WindowAdapter
import java.awt.event.WindowEvent
import java.io.File
import javax.imageio.ImageIO
import javax.swing.*
import javax.swing.filechooser.FileNameExtensionFilter
import kotlin.system.exitProcess

object MainScreen : JFrame("NoteKraken $buildNumber [Untitled]") {
    val mainPanel = JPanel()
    val menuBar = JMenuBar()
    val notes = ArrayList<JSONObject>()
    val notePane = JPanel()
    val scrollpane = JScrollPane(notePane)
    val searchBar = JTextField()
    val searchModes = arrayOf("Tag","Title","Content")
    val searchByDropdown = JComboBox(searchModes)
    val activeKeyCodes = HashSet<Int>()
    val dirChooser = object : JFileChooser(UserData.savePath){
        override fun updateUI() {
            updateTheme()
            super.updateUI()
        }
    }

    init {
        isVisible = false
        isResizable = false
        layout = BorderLayout()
        mainPanel.layout = BorderLayout()
        mainPanel.size = Dimension(650,650)
        mainPanel.preferredSize = Dimension(650,650)
        defaultCloseOperation = DO_NOTHING_ON_CLOSE

        constructMenuBar()
        constructTopPanel()
        constructMainArea()
        add(menuBar, BorderLayout.NORTH)
        add(mainPanel,BorderLayout.CENTER)

        KeyboardFocusManager.getCurrentKeyboardFocusManager().addKeyEventDispatcher { event ->
            if(event.isControlDown && event.keyCode == 83){
                saveNotes()
                return@addKeyEventDispatcher true
            }
            return@addKeyEventDispatcher false
        }

        pack()
        SwingUtilities.invokeLater {
            updateTheme()
            UserData.parse()
        }


        Runtime.getRuntime().addShutdownHook(Thread {
            UserData.save()
        })

        addWindowListener(object : WindowAdapter() {
            override fun windowClosing(p0: WindowEvent?) {
                if(unsavedChanges){
                    ExitPrompt { shouldSave ->
                        if(shouldSave){
                            saveNotes()
                        }
                        exitProcess(0)
                    }.open()
                } else {
                    exitProcess(0)
                }
            }
        })

        iconImage = ImageIO.read(this::class.java.getResource("/logo.png"))
        setLocationRelativeTo(null)
    }

    fun constructMainArea(){
        notePane.layout = BoxLayout(notePane, BoxLayout.PAGE_AXIS)
        scrollpane.verticalScrollBar.unitIncrement = 15

        if(notes.isNotEmpty())
            for(note in notes)
                notePane.add(NotePreview(note)).also { notePane.add(JSeparator(JSeparator.HORIZONTAL)) }

        mainPanel.add(scrollpane, BorderLayout.CENTER)
    }

    fun constructTopPanel(){
        val topBar = JPanel(BorderLayout())

        //⊞
        //🖉
        val newNoteButton = JButton("＋")
        newNoteButton.addActionListener {
            val j = JSONObject()
            notes.add(j)
            NoteEditor(j).open()
        }
        newNoteButton.size = Dimension(50,50)

        searchBar.size = Dimension(width - 120, 30)
        searchBar.minimumSize = Dimension(width - 120, 30)
        searchBar.maximumSize = Dimension(width - 120, 30)

        searchByDropdown.size = Dimension(70,30)
        searchByDropdown.minimumSize = Dimension(70,30)
        searchByDropdown.maximumSize = Dimension(70,30)

        val searchPanel = JPanel(BorderLayout())
        searchPanel.add(searchByDropdown, BorderLayout.WEST)

        searchBar.addActionListener {
            updateNotes()
        }

        val searchButton = JButton("\uD83D\uDD0D")
        searchButton.size = Dimension(50,50)
        searchButton.addActionListener {
            updateNotes()
        }

        searchPanel.add(searchButton, BorderLayout.EAST)

        topBar.add(newNoteButton,BorderLayout.WEST)
        topBar.add(searchBar, BorderLayout.CENTER)
        topBar.add(searchPanel, BorderLayout.EAST)
        topBar.add(JLabel("        "), BorderLayout.SOUTH)

        mainPanel.add(topBar, BorderLayout.NORTH)
    }

    fun constructMenuBar() {
        val fileMenu = JMenu("File")
        val newSubmenu = JMenu("New")
        val saveMenuItem = JMenuItem("Save")
        val openMenuItem = JMenuItem("Open")
        val importItem = JMenuItem("Import")
        val exportSubmenu = JMenu("Export")
        val aboutItem = JMenuItem("About")

        aboutItem.addActionListener {
            AboutWindow.open()
        }

        importItem.addActionListener {
            dirChooser.fileFilter = FileNameExtensionFilter("JSON FILES","json")
            val response = dirChooser.showOpenDialog(null)
            if(response == JFileChooser.APPROVE_OPTION){
                parseNotes(dirChooser.selectedFile.absolutePath)
            }
        }

        openMenuItem.addActionListener {
            dirChooser.fileFilter = FileNameExtensionFilter("JSON FILES","json")
            val response = dirChooser.showOpenDialog(null)
            if(response == JFileChooser.APPROVE_OPTION){
                if(unsavedChanges){
                    ExitPrompt { shouldSave ->
                        if(shouldSave){
                            saveNotes()
                        }
                        notes.clear()
                        UserData.savePath = dirChooser.selectedFile.absolutePath
                        parseNotes(UserData.savePath)
                        unsavedChanges = false
                        updateTitle()
                    }.open()
                } else {
                    notes.clear()
                    UserData.savePath = dirChooser.selectedFile.absolutePath
                    parseNotes(UserData.savePath)
                    unsavedChanges = false
                    updateTitle()
                }
            }
        }

        saveMenuItem.addActionListener {
            saveNotes()
        }

        val newNoteItem = JMenuItem("Note")
        newNoteItem.addActionListener {
            val j = JSONObject()
            notes.add(j)
            NoteEditor(j).open()
        }
        val newDatabaseItem = JMenuItem("Database")
        newDatabaseItem.addActionListener {
            if(unsavedChanges){
                ExitPrompt { shouldSave ->
                    if(shouldSave){
                        saveNotes()
                    }
                    notes.clear()
                    UserData.savePath = ""
                    title = "NoteKraken $buildNumber [Untitled]"
                    updateNotes()
                }.open()
            } else {
                notes.clear()
                UserData.savePath = ""
                title = "NoteKraken $buildNumber [Untitled]"
                updateNotes()
            }

            unsavedChanges = false
        }
        newSubmenu.add(newNoteItem)
        newSubmenu.add(newDatabaseItem)

        val exportByTagItem = JMenuItem("By Tag")
        val exportAll = JMenuItem("All")
        exportSubmenu.add(exportByTagItem)
        exportSubmenu.add(exportAll)

        exportByTagItem.addActionListener {

        }

        fileMenu.add(newSubmenu)
        fileMenu.add(saveMenuItem)
        fileMenu.add(openMenuItem)
        fileMenu.add(importItem)
        fileMenu.add(exportSubmenu)
        fileMenu.add(aboutItem)

        menuBar.add(fileMenu)

        add(menuBar)
    }

    @JvmStatic
    fun main(args: Array<String>) {
        isVisible = true
    }
}