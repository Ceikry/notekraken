import java.awt.Color
import java.awt.event.MouseEvent
import java.awt.event.MouseListener
import javax.swing.BorderFactory
import javax.swing.JLabel
import javax.swing.border.EtchedBorder

class TagLabel(tagName: String, canInteract: Boolean = false) : JLabel(tagName.trim()) {
    init {

        border = BorderFactory.createRaisedBevelBorder()

        if(canInteract){
            addMouseListener(object : MouseListener{
                override fun mouseClicked(p0: MouseEvent?) {
                    MainScreen.searchBar.text = tagName
                    updateNotes()
                }
                override fun mousePressed(p0: MouseEvent?) {}
                override fun mouseReleased(p0: MouseEvent?) {}
                override fun mouseEntered(p0: MouseEvent?) {}
                override fun mouseExited(p0: MouseEvent?) {}
            })
        }
    }
}