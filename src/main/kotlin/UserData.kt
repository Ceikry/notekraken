import org.json.simple.JSONObject
import org.json.simple.parser.JSONParser
import java.io.File
import java.io.FileReader
import java.io.FileWriter

private val SAVE_PATH = System.getProperty("user.home") + File.separator + ".notekraken"
private val fileName = "userPrefs.json"
object UserData {
    var savePath = ""

    fun save(){
        val o = JSONObject()
        o["lastPath"] = savePath

        if(!File(SAVE_PATH).exists()) File(SAVE_PATH).mkdirs()
        FileWriter(SAVE_PATH + File.separator + fileName).use {
            it.write(o.toJSONString())
        }
    }

    fun parse() {
        if(File(SAVE_PATH + File.separator + fileName).exists()) {
            val reader = FileReader(SAVE_PATH + File.separator + fileName)
            val data = JSONParser().parse(reader) as JSONObject

            savePath = data.getOrDefault("lastPath", "").toString()
            if (!savePath.isEmpty()) parseNotes(savePath).also { updateTitle() }
        }
    }
}