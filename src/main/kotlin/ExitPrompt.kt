import java.awt.BorderLayout
import java.awt.Dimension
import java.awt.FlowLayout
import javax.imageio.ImageIO
import javax.swing.JButton
import javax.swing.JFrame
import javax.swing.JLabel
import javax.swing.JPanel
import javax.swing.border.Border
import kotlin.system.exitProcess

class ExitPrompt(val callback: (Boolean) -> Unit) : JFrame("Warning") {

    init {
        isVisible = false
        layout = BorderLayout()
        isResizable = false
        preferredSize = Dimension(250,120)
        minimumSize = Dimension(250,120)
        maximumSize = Dimension(250,120)
        isAlwaysOnTop = true

        val warningPanel = JPanel(FlowLayout())

        val warningLabel = JLabel("<html><div style='text-align:center;'>You have unsaved changes.<br/>Save them now?<br/></div></html>")

        warningPanel.add(warningLabel)

        add(warningPanel, BorderLayout.CENTER)

        val buttonPanel = JPanel(FlowLayout())

        val yesButton = JButton("Yes")
        yesButton.addActionListener {
            isVisible = false
            callback.invoke(true)
        }

        val noButton = JButton("No")
        noButton.addActionListener {
            isVisible = false
            callback.invoke(false)
        }

        buttonPanel.add(yesButton)
        buttonPanel.add(JLabel("    "))
        buttonPanel.add(noButton)

        add(buttonPanel, BorderLayout.SOUTH)

        pack()
        updateTheme()
        iconImage = ImageIO.read(this::class.java.getResource("/logo.png"))
        setLocationRelativeTo(MainScreen)
    }

    fun open(){
        isVisible = true
    }
}