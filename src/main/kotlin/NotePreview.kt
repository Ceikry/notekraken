import org.json.simple.JSONObject
import java.awt.BorderLayout
import java.awt.Dimension
import java.awt.FlowLayout
import javax.swing.*

class NotePreview(note: JSONObject) : JPanel(BorderLayout()) {
    var maxNoteLength = 425
    init {
        val contentPanel = JPanel(BorderLayout())
        size = Dimension(630, 130)
        preferredSize = Dimension(630, 130)
        minimumSize = Dimension(630,130)
        maximumSize = Dimension(630, 130)

        val title = note.getOrDefault("title","").toString()
        var content = note.getOrDefault("content","").toString()
        val tags = note.getOrDefault("tags","").toString().split(",")

        val titleLabel = JLabel("<html><B>$title</B></html>")
        if(content.length > maxNoteLength)
            content = content.substring(0,maxNoteLength)
        val contentLabel = JTextPane()
        contentLabel.contentType = "text/html"
        contentLabel.text = "<html> <p style='color:white;font-family:Arial Narrow, sans-serif;font-size:95%;'>${content.replace("\n","<br/>")}</p></html>"
        contentLabel.isEditable = false

        val tagPanel = JPanel(FlowLayout())
        tagPanel.add(JLabel("Tags: "))
        for(tag in tags){
            tagPanel.add(TagLabel(tag, true))
        }

        contentPanel.add(titleLabel, BorderLayout.NORTH)
        contentPanel.add(contentLabel, BorderLayout.CENTER)
        contentPanel.add(tagPanel, BorderLayout.SOUTH)

        val buttonPanel = JPanel(BorderLayout())
        val viewButton = JButton("\uD83D\uDD89")
        viewButton.size = Dimension(50,50)
        viewButton.preferredSize = Dimension(50,50)
        viewButton.minimumSize = Dimension(50,50)
        viewButton.maximumSize = Dimension(50,50)
        viewButton.addActionListener {
            NoteEditor(note).open()
        }
        buttonPanel.add(viewButton)

        add(contentPanel,BorderLayout.CENTER)
        add(buttonPanel,BorderLayout.EAST)
    }
}