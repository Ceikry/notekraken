import com.github.weisj.darklaf.LafManager
import com.github.weisj.darklaf.theme.OneDarkTheme
import org.json.simple.JSONArray
import org.json.simple.JSONObject
import org.json.simple.parser.JSONParser
import java.awt.Image
import java.awt.image.BufferedImage
import java.io.File
import java.io.FileReader
import java.io.FileWriter
import javax.swing.JFileChooser
import javax.swing.JSeparator
import javax.swing.SwingUtilities


const val buildNumber = "0.90"
val tagCache = HashSet<String>()
var unsavedChanges = false

fun updateTheme() {
    LafManager.install(OneDarkTheme())
}

fun updateTitle(){
    val file = if(UserData.savePath.isEmpty()) "Untitled" else File(UserData.savePath).nameWithoutExtension
    MainScreen.title = "NoteKraken $buildNumber [$file${if(unsavedChanges) "*" else ""}]"
}

fun updateNotes() {
    MainScreen.notePane.removeAll()
    val filter = getFilteredNotes()
    filter.forEach {
        MainScreen.notePane.add(NotePreview(it)).also { MainScreen.notePane.add(JSeparator(JSeparator.HORIZONTAL)) }
    }
    MainScreen.revalidate()
    MainScreen.repaint()
    SwingUtilities.invokeLater {
        MainScreen.scrollpane.verticalScrollBar.value = 0
    }
}

fun getFilteredNotes(): List<JSONObject> {
    val list = ArrayList<JSONObject>()

    if(MainScreen.searchBar.text.isEmpty()) return MainScreen.notes

    when(MainScreen.searchByDropdown.selectedIndex){
        0 -> return MainScreen.notes.filter { it["tags"].toString().toLowerCase().contains(MainScreen.searchBar.text.toLowerCase()) }.toList()
        1 -> return MainScreen.notes.filter { it["title"].toString().toLowerCase().contains(MainScreen.searchBar.text.toLowerCase()) }.toList()
        2 -> return MainScreen.notes.filter { it["content"].toString().toLowerCase().contains(MainScreen.searchBar.text.toLowerCase()) }.toList()
    }

    return ArrayList<JSONObject>()
}

fun saveNotes() {
    if(UserData.savePath.isEmpty()) {
        val response = MainScreen.dirChooser.showSaveDialog(null)
        if(response == JFileChooser.APPROVE_OPTION) {
            var f = MainScreen.dirChooser.selectedFile
            if (!f.extension.equals("json", true)) {
                f = File(f.absolutePath + ".json")
            }
            UserData.savePath = f.absolutePath
            write()
            updateTitle()
        }
    } else {
        write()
        updateTitle()
    }
}

private fun write() {
    FileWriter(UserData.savePath).use { writer ->
        val ja = JSONArray()
        MainScreen.notes.map { ja.add(it) }
        writer.write(ja.toJSONString())
        unsavedChanges = false
    }
}

fun parseNotes(path: String){
    val reader = FileReader(path)
    val parser = JSONParser().parse(reader)
    val data = parser as JSONArray

    for(i in data){
        val o = i as JSONObject
        val tagSet = o.getOrDefault("tags","").toString().split(",").map { it.toLowerCase();it.trim() }.toList()
        tagCache.addAll(tagSet)
        MainScreen.notes.add(o)
    }

    updateNotes()
}

fun resize(img: BufferedImage, newW: Int, newH: Int): BufferedImage? {
    val tmp = img.getScaledInstance(newW, newH, Image.SCALE_SMOOTH)
    val dimg = BufferedImage(newW, newH, BufferedImage.TYPE_INT_ARGB)
    val g2d = dimg.createGraphics()
    g2d.drawImage(tmp, 0, 0, null)
    g2d.dispose()
    return dimg
}
