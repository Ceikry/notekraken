import org.json.simple.JSONObject
import java.awt.BorderLayout
import java.awt.Dimension
import javax.imageio.ImageIO
import javax.swing.*

class NoteEditor(val note: JSONObject) : JFrame("Edit Note") {
    init {
        isResizable = false
        isVisible = false
        size = Dimension(750,450)
        minimumSize = Dimension(750,450)
        maximumSize = Dimension(750,450)
        preferredSize = Dimension(750,450)
        layout = BorderLayout()

        val title = note.getOrDefault("title","").toString()
        val content = note.getOrDefault("content","").toString()
        val tags = note.getOrDefault("tags","").toString()

        val titlePanel = JPanel(BorderLayout())
        val titleLabel = JLabel("Title: ")
        val titleField = JTextField(title)
        titleField.minimumSize = Dimension(width - titleLabel.width - 5, 30)
        titleField.maximumSize = Dimension(width - titleLabel.width - 5, 30)
        titlePanel.add(titleLabel,BorderLayout.WEST)
        titlePanel.add(titleField,BorderLayout.CENTER)
        titlePanel.add(JLabel("\n\n\n"), BorderLayout.SOUTH)

        add(titlePanel,BorderLayout.NORTH)

        val contentPanel = JPanel(BorderLayout())
        val contentLabel = JLabel("Content")
        val contentField = JTextArea(content)
        val contentScrollpane = JScrollPane(contentField)
        contentField.lineWrap = true
        contentField.size = Dimension(490,260)
        contentPanel.add(contentLabel, BorderLayout.NORTH)
        contentPanel.add(contentScrollpane,BorderLayout.CENTER)
        contentPanel.add(JLabel("\n\n\n"), BorderLayout.SOUTH)

        add(contentPanel,BorderLayout.CENTER)

        val tagPanel = JPanel(BorderLayout())
        val tagLabel = JLabel("Tags (Max 10):")
        val tagField = JTextField(tags)
        val saveButton = JButton("Save Changes")

        saveButton.addActionListener {
            note["title"] = titleField.text
            note["content"] = contentField.text
            var tagString = tagField.text.split(",").map { it.toLowerCase().trim() }.toTypedArray()
            if(tagString.size > 10) tagString = tagString.copyOfRange(0,10)
            note["tags"] = tagString.joinToString(",").also { println("$it") }

            isVisible = false
            unsavedChanges = true
            updateTitle()
            updateNotes()
        }

        tagField.maximumSize = Dimension(width - tagLabel.width - 5, 30)
        tagField.minimumSize = Dimension(width - tagLabel.width - 5, 30)

        tagPanel.add(tagLabel, BorderLayout.WEST)
        tagPanel.add(tagField, BorderLayout.CENTER)
        tagPanel.add(saveButton, BorderLayout.SOUTH)

        add(tagPanel, BorderLayout.SOUTH)

        pack()
        SwingUtilities.invokeLater {
            updateTheme()
        }

        iconImage = ImageIO.read(this::class.java.getResource("/logo.png"))

        setLocationRelativeTo(MainScreen)
    }

    fun open(){
        isVisible = true
    }
}