<h1>NoteKraken</h1>
<img src="NoteKraken.png" alt="drawing" width="200"/>

<h2>About</h2>
NoteKraken is a project I made mostly for myself to use due to a lack of what I saw as adequate<br/>
note-taking software. It is currently in active development in its early stages still, but its current <br/>
features are as follows:

<ul>

- Tag-based note taking: All notes support tags and tags can be used to sort and categorize notes.
- Tags can be clicked on to automatically shift the note filter to notes with that tag.
- Notes can be searched by tag, content and title.
- Notes are saved in a portable JSON format.
- Import databases of notes into other databases of notes, allowing separate note<br/>
databases to be combined.
</ul>

Now keep in mind my intentions are to keep this tool extremely simple. However, there are a couple more planned features to come:

<ul>

- Export-by-tag: Export all notes by a particular tag to make sharing notes related to a<br/>
certain subject easier and more streamlined to do.
- Tag quick-access menu: Menu to quickly access any tag without having to remember<br/>
its name.
</ul>

The goal of NoteKraken will always be to be as simple and maintainable as it can be while delivering <br/>
the features it needs to be a complete piece of software for my own use. 

<h2>Installing</h2>
<b>Note: NoteKraken requires a version of Java installed to run. </b>
<h3>Universal</h3>
NoteKraken is written in Kotlin, and comes built as a .jar. The latest build of this universal Jar file<br/>
can be downloaded [here](https://gitlab.com/Ceikry/NoteKraken/-/jobs/artifacts/master/raw/build/libs/NoteKraken.jar?job=build). 

<h3>Linux Only</h3>
I am a Linux user myself, and as a result I suppose Linux users get special attention here.<br/>
Linux users can clone the repo, or download it as a zip, and run the bundled `install.sh`<br/>
script. This will automatically build the .jar straight from the source code, and then copy it<br/>
as well as a pre-made `.desktop` file to the appropriate locations. This allows you to have<br/>
the full, fancy, desktop program experience if that's what you'd desire.

<br/><br/>
<h4>Legalese</h4>
The logo is the result of combining and refining 2 pre-existing assets. As far as I'm aware, <br/>
the notepad graphic is public domain. The original version of the octopus we used is licensed<br/>
CCBY Grégory Montigny, FR. Many thanks to him for the awesome octopus/kraken.